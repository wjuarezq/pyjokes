#!/usr/bin/python
#
# Differences between 2 directories (not ready yet) 
# 
# Now (2012-10-24) alpha version doing "./diffdir [dir1] [dir2]
# Now (2012-10-19) create a dictionary with all files and folders 
#
# TODO:
#    - sort results
#
# HOW TO RUN:
# $ ./diffdir [directory1] [directory2]
#

import types
import math
def create_dict_aux(curr_path, curr_dir):
    path = curr_path + os.sep + curr_dir
    if os.path.islink(path):
        return 'link'
    if not os.path.isdir(path):
        return str(os.path.getsize(path))
    else:
        result = {}
        for elem in os.listdir(path):
            #result[path + os.sep + elem] = create_dict_aux(path, elem)
            result[elem] = create_dict_aux(path, elem)
        return result
    
def create_dict(curr_file):
    splitted = curr_file.split(os.path.sep)
    name = splitted[-2:-1][0] if splitted[-1:][0] == '' else splitted[-1:][0]
    result = { name : create_dict_aux(curr_file, '')}
    return result[result.keys()[0]]
    
def diff_dict(dict1, dict2):
    result = []
    for elem in dict1:
        diff_dict_aux('', elem, dict1, dict2, result)
    for elem in dict2:
        print_missing("['" + elem + "']", dict2, result)
    to_return = '\n'.join(result)
    to_return = to_return.replace("'", '').replace(']', '').replace('[', os.sep)
    return to_return
    
def diff_dict_aux(path, curr, dict1, dict2, result):
    #print('args: p:%s, curr:%s, res:%s' % (path, curr, result))
    import types, os
    #dict_args = "%s['%s']" % (path, curr)
    dict_args = path + "['" + curr + "']"
    #dict_args_formatted = dict_args.replace("'", '').replace('[', '').replace(']', os.sep)[:-1]
    #print(dict_args)
    str1 = "dict1" + dict_args
    #print(str1)
    elem1 = eval(str1)
    str2 = "dict2" + dict_args
    elem2 = ''
    try:
        elem2 = eval(str2)
    except: 
        #result.append('+ %s%s' % (dict_args_formatted, os.sep if isinstance(elem1, types.DictType) else ''))
        #result.append('+ %s%s' % (dict_args, os.sep if isinstance(elem1, types.DictType) else ''))
        result.append("+ " + dict_args + str(os.sep if isinstance(elem1, types.DictType) else ""))
        return
    if isinstance(elem1, types.StringTypes):
        if isinstance(elem2, types.StringTypes):
            #if elem1 != elem2:
            result.append('= ' + dict_args)
            delete_dict(dict_args, dict2)
            """
            if abs(long(elem1) - long(elem2)) >= 25:
                #result.append('! diff %s' % dict_args_formatted)
                result.append('! diff ' + dict_args)
            """
        else:
            #result.append('! types %s' % dict_args_formatted)
            result.append('! types ' + dict_args + '\n' + \
                          '    < file\n' + \
                          '    > dir')
            delete_dict(dict_args, dict2)
    else:
        # Then elem1 is a dictionary, so a directory
        if not isinstance(elem2, types.DictType):
            #result.append('! types %s' % dict_args_formatted)
            result.append('! types ' + dict_args + '\n' + \
                          '    < dir\n' + \
                          '    > file')
            delete_dict(dict_args, dict2)
            return
        # Then both are directories
        #tmp_dict_1 = eval('dict1%s' % elem1)
        #tmp_dict_2 = eval('dict2%s' % elem2)
        for e in elem1:
            diff_dict_aux(dict_args, e, dict1, dict2, result)
            delete_dict(dict_args + "['" + e + "']", dict2)
        print_missing(dict_args, dict2, result)

def delete_dict(dict_args, dict2):
    str = "dict2" + dict_args
    try:
        exec("del " + str)
    except:
        pass
        print >> sys.stderr, "ERR: couldn't delete %s" % str

def print_missing(dict_args, dict2, result):
    #print(dict_args)
    try: 
        elem2 = eval("dict2" + dict_args)
    except:
        return
    if isinstance(elem2, types.StringTypes):
        result.append("- " + dict_args)
        return
    for e in elem2:
        curr = dict_args + "['" + e + "']"
        if isinstance(elem2[e], types.DictType):
            print(curr + " is a dictionary")
            result.append("- " + curr + os.sep)
        print_missing(curr, dict2, result)
            
if __name__ == '__main__':
    import os
    import sys
    #from pprint import pprint as pp
    #old_stdout = sys.stdout
    #sys.stdout = open('C:\\Users\\admin\\log.txt', 'w')
    #out = open('C:\\Users\\admin\\log.txt', 'w')
    if len(sys.argv) == 3:
        dict1 = create_dict(sys.argv[1])
        dict2 = create_dict(sys.argv[2])
        result = diff_dict(dict1, dict2)
        print(result)
        #out.write(result)
        #pp(result)
        #pp(dict2)
    else:
        print('Usage: "diffdir [directory1] [directory2]"')
