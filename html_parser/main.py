from HTMLParser import HTMLParser as prs

# create a subclass and override the handler methods
class MyHTMLParser(prs):
    
    def handle_starttag(self, tag, attrs):
        #print "tag:", tag
        for attr in attrs:
            if attr[0] == 'class':
                print ('line:%s, char:%s, tag:%s, class:%s'
                       % (self.getpos()[0], self.getpos()[1], tag, attr[1]))
            
    #def handle_endtag(self, tag):
        #print "Encountered an end tag :", tag
        
    #def handle_data(self, data):
        #print "Encountered some data  :", data
        

# instantiate the parser and fed it some HTML
parser = MyHTMLParser()
f = open('./resources/pythonhelp.html')
#parser.feed('<html><head><title>Test</title></head>'
#            '<body><h1 clasS="contentnode">Parse me!</h1></body></html>')
parser.feed(f.read())
#print (parser.get_starttag_text()) 

from bs4 import BeautifulSoup as bs
import bs4
html_doc = """
 <html><head><title>The Dormouse's story</title></head>

 <p class="title"><b>The Dormouse's story</b></p>

 <p class="story">Once upon a time there were three little sisters; and their names were
 <a href="http://example.com/elsie" class="contentnode" id="link1">Elsie</a>,
 <a href="http://example.com/lacie" class="sister" id="link2">Lacie</a> and
 <a href="http://example.com/tillie" class="sister" id="link3">Tillie</a>;
 and they lived at the bottom of a well.</p>
 <div class="contentnode">
    <p class="ip">internal p</p>
    <div class="idiv">internal div</div>
 </div>

 <p class="story">...</p>
 """

s = bs(html_doc)
print(s.prettify())
print("-------------------")
for elem in s.select(".contentnode"):
    bs_elem = bs('%s' % elem)
    #print('id: %s' % isinstance(elem, bs4.element.Tag))
    print('%s' % elem.attrs['id'] if 'id' in elem.attrs else 'No id!')
    #print ("elem: \n%s" % bs_elem.prettify())