#!/usr/bin/python

class A(object): 
    allow = []
    def __init__(self): 
        pass
    def pr(self):
        for elem in self.allow: 
            print ('%s' % (self.__dict__[elem] if elem in self.__dict__.keys() else 'None'))

class B(A): 
    allow = ['f1', 'f2', 'f3']
    def __init__(self): 
        self.f1 = 'first'
        self.f2 = 'second'
    def pr(self):
        super(B,self).pr()

alh = B()
alh.pr()
