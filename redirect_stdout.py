#!/usr/bin/python
#
# Example about how to create a class and assign it 
# to 'sys.stdout' and customize 'print' instruction.
# 
# Redirect 'sys.stdout' to a file.
#
# HOW TO RUN:
# type "$ python" and then :
# >>> import sys
# >>> sys.path.append('/Users/admin/workspaces') # or wherever this script lives.
# >>> import redirect_stdout
#
import sys
import random
# sys.path.append('/Users/admin/workspaces')

class WritableObject: 
    
    def __init__(self):
        self.a = ''
        self.b = open('/Users/admin/scripts/log.txt', 'a')
    
    def write(self, string):
        """
        self.a = self.a + str(random.random()) + ": " + str(self.__dict__) + " " + string + "\n"
        self.b.write(str(self._to_dict()))
        """
        self.b.write("%s : %s\n" %(random.random(), self.__dict__))
        #self.b.flush()
        #self.b.write(str(random.random()))
    
    def _to_dict(self):
        return {'a' : self.a}
    

foo = WritableObject()
sys.stdout = foo

"""
def trick():
    print("%s : %s" % (random.random(), foo.to_dict()))
"""
